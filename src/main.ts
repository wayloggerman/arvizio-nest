import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as cookieParser from 'cookie-parser';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: {
      origin: [
        'http://localhost:5000',
        'http://192.167.1.220:5000',
        'http://192.168.1.193:5000',
        'http://127.0.0.1:5000',
        'https://arvizio.samaritanit.ru',
        'https://360x.arvizio.ru',
        'https://360x.arvizio.com',
        'http://360x.arvizio.com',
      ],
      credentials: true,
    },
    logger: ['error', 'warn', 'debug', 'log', 'verbose'],
  });
  app.use(cookieParser('secret', {}));

  const config = new DocumentBuilder()
    .setTitle('arvizio-backend-open-api')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  app.useGlobalPipes(new ValidationPipe());

  await app.listen(5001);
}
bootstrap();
