import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import postgresConfig from './config/postgres.config';
import { LoggerMiddleware } from './middleware/logger.midleware';
import { MailerService } from './modules/mailer/mailer.service';
import { UsersModule } from './modules/users/users.module';
import { MailerModule } from './modules/mailer/mailer.module';
import { ProjectModule } from './modules/project/project.module';
import { MediaModule } from './modules/media/media.module';
import { FilesModule } from './modules/files/files.module';
import { SpotsModule } from './modules/spots/spots.module';
import { UploaderModule } from './modules/uploader/uploader.module';
import { DownloaderModule } from './modules/downloader/downloader.module';
import { MediaTransformerModule } from './media-transformer/media-transformer.module';

import * as sqlite3 from 'sqlite3';

function createEnvPath() {
  let envFilePath = `./src/environment/`;

  switch (process.env.NODE_ENV) {
    case 'production':
      envFilePath += `.env.production`;
      break;
    case 'development':
      envFilePath += `.env.development`;
      break;
    default:
      envFilePath += `.env.local`;
      break;
  }

  console.log({ envFilePath });
  return envFilePath;
}

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: createEnvPath(),
      isGlobal: true,
      load: [postgresConfig],
    }),

    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        console.log({
          user: configService.get('postgres.user'),
          password: configService.get('postgres.password'),
        });

        return {
          type: 'postgres',
          host: configService.get('postgres.host'),
          port: configService.get('postgres.port'),
          username: configService.get('postgres.user'),
          password: configService.get('postgres.password'),
          database: configService.get('postgres.database'),
          // type: 'sqlite',
          // database: 'db/database.sqlite',
          synchronize: true,
          autoLoadEntities: true,
        };
      },
    }),

    UsersModule,

    MailerModule,

    ProjectModule,

    MediaModule,

    FilesModule,

    SpotsModule,

    UploaderModule,

    DownloaderModule,

    MediaTransformerModule,
  ],
  controllers: [AppController],
  providers: [AppService, ConfigService, MailerService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('*');
  }
}
