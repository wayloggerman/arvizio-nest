import { IsEmail, IsString } from 'class-validator';

export class SendMailDto {
  @IsString()
  @IsEmail()
  to: string;

  @IsString()
  subject: string;

  @IsString()
  text: string;

  @IsString()
  html: string;
}
