import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    console.log({
      // method: req.method,
      // url: req.url,
      // path: req.path,
      // body: req.body,
      // query: req.query,
      param: req.params,
    });

    next();
  }
}
