import { Module } from '@nestjs/common';
import { ImageTransformerService } from './services/image-transformer.service';
import { VideoTransformerService } from './services/video-transformer.service';

@Module({
  providers: [VideoTransformerService, ImageTransformerService],
  exports: [VideoTransformerService, ImageTransformerService],
})
export class MediaTransformerModule {}
