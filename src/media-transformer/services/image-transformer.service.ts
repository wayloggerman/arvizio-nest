import { Injectable } from '@nestjs/common';
import * as sharp from 'sharp';

@Injectable()
export class ImageTransformerService {
  async createThumbnail(
    imagePath: string,
    suffix = 'thrumblail',
    size = 320,
  ): Promise<string> {
    const imageFolder = imagePath.split('/').slice(0, -1).join('/');
    const imageName = imagePath.split('/').pop();
    const trumbName = `${imageName}-${suffix}.png`;
    const trumbPath = `${imageFolder}/${trumbName}`;

    await sharp(imagePath).resize(size).toFile(trumbPath);

    return trumbPath;
  }
}
