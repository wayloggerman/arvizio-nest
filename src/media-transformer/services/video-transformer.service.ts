import { Injectable } from '@nestjs/common';
import * as FFmpeg from 'fluent-ffmpeg';

@Injectable()
export class VideoTransformerService {
  async createScreenshot(
    videoPath: string,
    suffix = 'thrumblail',
    size = '320x240',
  ): Promise<string> {
    const videoFolder = videoPath.split('/').slice(0, -1).join('/');
    const videoName = videoPath.split('/').pop();
    const trumbName = `${videoName}-${suffix}.png`;
    return new Promise((resolve, reject) => {
      FFmpeg(videoPath)
        .screenshots({
          timestamps: ['50%'],
          filename: trumbName,
          folder: videoFolder,
          size,
        })

        .on('end', () => {
          const res = `${videoFolder}/${trumbName}`;

          resolve(res);
          return res;
        });
    });
  }
}
