import { Project } from 'src/modules/project/entity/project.entity';
import { User } from 'src/modules/users/entity/user.entity';
import {
  Column,
  Entity,
  Generated,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { MediaType } from './media.enum';

@Entity('media')
export class Media {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'varchar',
  })
  type: MediaType;
  @Column({
    type: 'varchar',
    length: 255,
    name: 'name',
    nullable: true,
  })
  name: string;
  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn({ name: 'owner' })
  owner: {
    id: number;
  };
  @Column({
    name: 'created_at',
    type: 'varchar',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;
  @Column({
    name: 'updated_at',
    type: 'varchar',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;
  @Column({
    name: 'deleted_at',
    type: 'varchar',
    nullable: true,
  })
  deletedAt: Date;

  @Column({
    type: 'boolean',
    default: false,
  })
  public: boolean;

  @Column({
    type: 'integer',
    default: 0,
  })
  size: number;

  @ManyToOne(() => Project, (project) => project.id)
  @JoinColumn({ name: 'project' })
  project:
    | {
        id: number;
        deletedAt: string;
        public: boolean;
        owner: {
          id: number;
        };
      }
    | number;

  @Column({
    name: 'description',
    type: 'varchar',
    length: 8096,
  })
  description: string;

  @Column({
    name: 'order',
    type: 'integer',
  })
  order: number;

  @Column({
    name: 'settings',
    type: 'jsonb',
    default: '{}',
  })
  settings: {
    numberOfLines: number;
  };
}
