export enum MediaType {
  image = 'image',
  video = 'video',
  audio = 'audio',
  panorama = 'panorama',
  model3d = 'model3d',
  pseudo3d = 'pseudo3d',
  logo = 'logo',
}
