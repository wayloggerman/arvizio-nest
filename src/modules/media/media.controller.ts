import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  Req,
} from '@nestjs/common';
import { MediaService } from './media.service';
import { CreateMediaDto } from './dto/create-media.dto';
import { UpdateMediaDto } from './dto/update-media.dto';
import { AuthGuard } from 'src/guards/auth.guard';
import ReadMediaDTO from './dto/read-media.dto';

@Controller('media')
@UseGuards(AuthGuard)
export class MediaController {
  constructor(private readonly mediaService: MediaService) {}

  @Post()
  create(@Req() request: any, @Body() createMediaDto: CreateMediaDto) {
    const userId = request.user.id;
    return this.mediaService.create(createMediaDto, userId);
  }

  @Get('all')
  findAll(@Req() request: any, @Query() query: ReadMediaDTO) {
    const userId = request.user.id;
    return this.mediaService.findAll(userId, query);
  }

  @Get()
  findOne(@Req() request: any, @Query('id') id: string) {
    const userId = request.user.id;

    return this.mediaService.findOne(+id, userId);
  }

  @Patch()
  update(@Req() request: any, @Body() updateMediaDto: UpdateMediaDto) {
    const userId = request.user.id;
    return this.mediaService.update(updateMediaDto, userId);
  }

  @Delete()
  remove(@Req() request: any, @Query('id') id: string) {
    const userId = request.user.id;
    return this.mediaService.remove(+id, userId);
  }
}
