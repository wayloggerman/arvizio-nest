import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IsNull, Repository } from 'typeorm';
import { Project } from '../project/entity/project.entity';
import { CreateMediaDto } from './dto/create-media.dto';
import ReadMediaDTO from './dto/read-media.dto';
import { UpdateMediaDto } from './dto/update-media.dto';
import { Media } from './entities/media.entity';
@Injectable()
export class MediaService {
  constructor(
    @InjectRepository(Media)
    private readonly mediaRepository: Repository<Media>,
  ) {}

  async create(createMediaDto: CreateMediaDto, userId: number) {
    const medias = await this.mediaRepository.find({});

    const res = await this.mediaRepository.insert({
      ...createMediaDto,
      order: medias.length,
      owner: {
        id: userId,
      },
      settings: {
        numberOfLines: 1,
      },
    });
    return res.identifiers[0].id;
  }
  async update(updateMediaDto: UpdateMediaDto, userId: number) {
    const res = await this.mediaRepository.update(
      { id: updateMediaDto.id, owner: { id: userId } },
      {
        ...updateMediaDto,
        settings: updateMediaDto.settings,
      },
    );
    return res;
  }
  async findAll(userId: number, dto: ReadMediaDTO) {
    const where = [];

    where.push({
      owner: {
        id: userId,
      },
      deletedAt: IsNull(),
      project: {
        deletedAt: IsNull(),
        id: parseInt(dto.projectId) ?? undefined,
      },
    });
    where.push({
      public: true,
      deletedAt: IsNull(),
    });

    const res = await this.mediaRepository.find({
      relationLoadStrategy: 'join',
      where,
      relations: {
        project: true,
      },
    });

    return res;
  }

  findOne(id: number, userId?: number) {
    const where = [];
    if (userId) {
      where.push({
        id,
        owner: {
          id: userId,
        },
        deletedAt: IsNull(),
      });
    }
    if (!userId) {
      where.push({
        id,
        deletedAt: IsNull(),
        project: {
          public: true,
          deletedAt: IsNull(),
        },
      });
    }
    return this.mediaRepository.findOne({
      where,
      relations: {
        owner: true,
        project: true,
      },
    });
  }

  remove(id: number, userId: number) {
    return Promise.all([
      this.mediaRepository.update(
        { id, owner: { id: userId } },
        { deletedAt: () => `CURRENT_TIMESTAMP` },
      ),
    ]);
  }
}
