import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDefined,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export default class ReadMediaDTO {
  @IsDefined()
  @IsOptional()
  @ApiPropertyOptional()
  projectId: string;
}
