import { ApiProperty, PartialType } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDefined,
  IsJSON,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { IsNull } from 'typeorm';
import { Media } from '../entities/media.entity';
import { CreateMediaDto } from './create-media.dto';

export class UpdateMediaDto extends Media {
  @IsDefined()
  @ApiProperty()
  @IsNumber()
  id: number;

  @IsOptional()
  @ApiProperty()
  @IsString()
  name: string;

  @IsOptional()
  @ApiProperty()
  @IsString()
  description: string;

  @IsOptional()
  @ApiProperty()
  @IsBoolean()
  public: boolean;

  @IsOptional()
  @ApiProperty()
  @IsNumber()
  size: number;

  @IsOptional()
  @ApiProperty()
  @IsNumber()
  order: number;

  @IsOptional()
  @ApiProperty()
  settings: {
    numberOfLines: number;
  };
}
