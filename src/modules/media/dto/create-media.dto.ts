import { ApiProperty, ApiPropertyOptional, PartialType } from '@nestjs/swagger';
import {
  IsDefined,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { Media } from '../entities/media.entity';
import { MediaType } from '../entities/media.enum';

export class CreateMediaDto extends Media {
  @ApiProperty()
  @IsDefined()
  @IsString()
  name: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  description: string;

  @ApiProperty()
  @IsDefined()
  @IsEnum(MediaType)
  type: MediaType;

  @ApiProperty()
  @IsNumber()
  @IsDefined()
  project: number;
}
