import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from 'src/modules/users/users.module';
import { File } from '../files/entity/file.entity';
import { FileService } from '../files/services/file.services';
import { Media } from './entities/media.entity';
import { MediaController } from './media.controller';
import { MediaService } from './media.service';

@Module({
  controllers: [MediaController],
  providers: [MediaService],
  imports: [UsersModule, TypeOrmModule.forFeature([Media]), File],
  exports: [MediaService],
})
export class MediaModule {}
