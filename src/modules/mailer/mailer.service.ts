import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as nodemailer from 'nodemailer';
import { SendMailDto } from 'src/dto/send-mail.dto';

@Injectable()
export class MailerService {
  constructor(private readonly configService: ConfigService) {}

  async confirmMail(email: string, token: string) {
    const html = `<div>
  <h1>Hello ${email} !</h1>
  <h3>
    this address used for registration on site
    <a href="www.arvizio.com">www.arvizio.com</a> , for confirm registration
    please click here: <a href="${this.configService.get(
      'url.userConfirm',
    )}?token=${token}">CONFIRM</a>
  </h3>
</div> `;

    return this.send({
      to: email,
      subject: 'Arvizio: подтверждение регистрации',
      html,
      text: '',
    });
  }

  async newPassword(email: string, password: string) {
    const html = `<div>
    <h1>Hello ${email} !</h1>
    <h3>
      you request new password for site 
      <a href="www.arvizio.com">www.arvizio.com</a> , 
      you are can use this password for access to site ${password}
    </h3>
  </div> `;

    return this.send({
      to: email,
      subject: 'Arvizio: новый пароль',
      html,
      text: '',
    });
  }

  private send(sendMailDto: SendMailDto) {
    const { to, subject, text, html } = sendMailDto;

    const transport = nodemailer.createTransport({
      host: this.configService.get('mail.host'),
      port: this.configService.get('mail.port'),
      secure: true,
      auth: {
        user: this.configService.get('mail.user'),
        pass: this.configService.get('mail.pass'),
      },
    });
    transport.sendMail({
      from: this.configService.get('mail.user'),
      to,
      subject,
      text,
      html,
    });
  }
}
