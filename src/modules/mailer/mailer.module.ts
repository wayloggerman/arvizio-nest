import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MailerService } from './mailer.service';
import mailConfig from '../../config/mail.config';
import urlConfig from 'src/config/url.config';

@Module({
  exports: [MailerService],
  providers: [MailerService],
  imports: [
    ConfigModule.forFeature(mailConfig),
    ConfigModule.forFeature(urlConfig),
  ],
})
export class MailerModule {}
