import { Media } from 'src/modules/media/entities/media.entity';
import { User } from 'src/modules/users/entity/user.entity';
import {
  Column,
  Entity,
  Generated,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { FileTypeEnum } from './file.enum';
import { Storage } from './storage.enum';

@Entity('file')
export class File {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'varchar',
    length: 255,

    nullable: true,
  })
  name: string;

  @Column({
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  path: string;

  @Column({
    type: 'integer',
    default: 0,
  })
  size: number;

  @ManyToOne(() => Media, (media) => media.id)
  @JoinColumn({ name: 'media' })
  media: {
    id: number;
    deletedAt: string;
    project: {
      id: number;
      deletedAt: string;
      public: boolean;
    };
  };
  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn({ name: 'owner' })
  owner: {
    id: number;
  };
  @Column({
    type: 'varchar',
    length: 255,
    default: Storage.LOCAL,
  })
  storage: Storage;

  @Column({
    name: 'created_at',
    type: 'varchar',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;
  @Column({
    name: 'updated_at',
    type: 'varchar',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;
  @Column({
    name: 'deleted_at',
    type: 'varchar',
    nullable: true,
  })
  deletedAt: Date;

  @Column({
    name: 'extension',
    type: 'varchar',
    length: 255,
    default: '.unknown',
    nullable: true,
  })
  extension: string;

  @Column({
    name: 'type',
    type: 'varchar',
    length: 255,
    default: FileTypeEnum.other,
  })
  type: string;

  @Column({
    name: 'settings',
    type: 'jsonb',
    default: '{}',
  })
  settings: {
    relativeColumn: number;
    relativeLine: number;
    // [key: string]: any;
  };
}
