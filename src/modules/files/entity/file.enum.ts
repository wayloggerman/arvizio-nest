export enum FileTypeEnum {
  image = 'image',
  trumb = 'trumb',
  video = 'video',
  audio = 'audio',
  document = 'document',
  other = 'other',
  panorama = 'panorama',
  pseudo3d = 'pseudo3d',
  model3d = 'model3d',
}
