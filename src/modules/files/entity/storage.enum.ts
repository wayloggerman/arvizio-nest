export enum Storage {
  LOCAL = 'local',
  S3 = 's3',
  GCS = 'gcs',
  Yandex = 'yandex',
}
