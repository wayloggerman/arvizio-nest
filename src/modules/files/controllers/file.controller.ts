import {
  Body,
  Controller,
  Delete,
  Get,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from 'src/guards/auth.guard';
import { CreateFileDTO } from '../dto/create-file.dto';
import ReadFileDTO from '../dto/read-file.dto';
import { UpdateFileDTO } from '../dto/update-file.dto';
import { FileService } from '../services/file.services';

@UseGuards(AuthGuard)
@Controller('file')
export class FileController {
  constructor(private readonly fileService: FileService) {}
  @Post()
  create(@Req() request: any, @Body() dto: CreateFileDTO) {
    return this.fileService.create(dto, request.user.id);
  }

  @Get()
  read(
    @Req() request: any,
    @Query('fileId') fileId: number,
    @Query('mediaId') mediaId: number,
  ) {
    const userId = request.user.id;
    return this.fileService.read(mediaId, fileId, userId);
  }
  @Get('all')
  readAll(@Req() request: any, @Query() query: ReadFileDTO) {
    const userId = request.user.id;
    return this.fileService.readAll(userId, query);
  }
  @Patch()
  update(@Req() request: any, @Body() dto: UpdateFileDTO) {
    const userId = request.user.id;
    return this.fileService.update(dto.id, dto, userId);
  }
  @Delete()
  delete(@Req() request: any, @Query('id') id: number) {
    const userId = request.user.id;
    return this.fileService.delete(id, userId);
  }
}
