import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MediaModule } from '../media/media.module';
import { UsersModule } from '../users/users.module';
import { FileController } from './controllers/file.controller';
import { File } from './entity/file.entity';
import { FileService } from './services/file.services';

@Module({
  imports: [TypeOrmModule.forFeature([File]), UsersModule, MediaModule],
  providers: [FileService],
  controllers: [FileController],
  exports: [FileService],
})
export class FilesModule {}
