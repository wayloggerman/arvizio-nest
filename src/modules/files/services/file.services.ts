import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IsNull, Repository } from 'typeorm';
import { CreateFileDTO } from '../dto/create-file.dto';
import ReadFileDTO from '../dto/read-file.dto';
import { UpdateFileDTO } from '../dto/update-file.dto';
import { File } from '../entity/file.entity';

@Injectable()
export class FileService {
  constructor(
    @InjectRepository(File)
    private readonly fileRepository: Repository<File>,
  ) {}

  async create(dto: CreateFileDTO, userId: number) {
    const res = await this.fileRepository.insert({
      name: dto.name,
      type: dto.type,
      path: dto.path,
      extension: dto.extension,
      size: dto.size,

      media: {
        id: dto.media,
      },
      owner: {
        id: userId,
      },
      settings: {
        relativeColumn: 0,
        relativeLine: 0,
      },
    });

    return res.identifiers[0].id;
  }

  update(id: number, dto: UpdateFileDTO, userId: number) {
    return this.fileRepository.update(id, {
      ...dto,
      owner: {
        id: userId,
      },
    });
  }
  async read(mediaId: number, fileId: number, userId: number) {
    const numFileId =
      typeof fileId === 'string' ? parseInt(fileId as string) : fileId;
    const numMediaId =
      typeof mediaId === 'string' ? parseInt(mediaId as string) : mediaId;

    const res = await this.fileRepository.findOne({
      relations: {
        media: true,
        owner: true,
      },
      where: {
        id: numFileId,
        owner: {
          id: userId,
        },
        media: {
          id: numMediaId,
        },
      },
    });
    return res;
  }
  async readByName(mediaId: number, fileName: string, userId: number) {
    const res = await this.fileRepository.findOne({
      relations: {
        media: true,
        owner: true,
      },
      where: {
        name: fileName,
        // owner: {
        // id: userId,
        // },
        media: {
          id: typeof mediaId === 'string' ? parseInt(mediaId) : mediaId,
        },
      },
    });

    return res;
  }
  readAll(userId: number, query: ReadFileDTO) {
    const where = [];

    where.push({
      media: {
        deletedAt: IsNull(),
        project: {
          deletedAt: IsNull(),
        },
        id: parseInt(query.mediaId) ?? undefined,
      },
      owner: {
        id: userId,
      },
      deletedAt: IsNull(),
    });

    return this.fileRepository.find({
      where,
      relations: {
        media: true,
      },
    });
  }

  delete(id: number, userId: number) {
    console.log({
      id,
      userId,
    });

    return this.fileRepository.update(
      {
        id,
        owner: {
          id: userId,
        },
      },
      { deletedAt: () => 'CURRENT_TIMESTAMP' },
    );
  }
}
