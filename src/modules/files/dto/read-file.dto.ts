import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDefined,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export default class ReadFileDTO {
  @IsDefined()
  @IsOptional()
  @ApiPropertyOptional()
  mediaId: string;
}
