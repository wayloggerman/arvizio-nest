import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNumber, IsOptional, IsString } from 'class-validator';
import { File } from '../entity/file.entity';
import { FileTypeEnum } from '../entity/file.enum';

export class CreateFileDTO {
  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsNumber()
  media: number;

  @ApiProperty()
  @IsEnum(FileTypeEnum)
  type: FileTypeEnum;

  @IsOptional()
  @IsString()
  path?: string;

  @IsOptional()
  @IsString()
  extension?: string;

  @IsOptional()
  @IsNumber()
  size?: number;

  @IsOptional()
  @IsString()
  storage?: string;
}
