import { ApiProperty, ApiPropertyOptional, PartialType } from '@nestjs/swagger';
import { IsDefined, IsJSON, IsNumber, IsOptional } from 'class-validator';
import { File } from '../entity/file.entity';
import { CreateFileDTO } from './create-file.dto';

export class UpdateFileDTO extends File {
  @ApiProperty()
  @IsNumber()
  @IsDefined()
  id: number;

  @IsOptional()
  @ApiPropertyOptional()
  @IsNumber()
  @IsDefined()
  extension: string;

  @IsOptional()
  @ApiProperty()
  settings: {
    relativeColumn: number;
    relativeLine: number;
    [key: string]: any;
  };
}
