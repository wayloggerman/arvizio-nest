import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';
import { User } from '../entity/user.entity';

export class DeleteUserDTO extends User {
  @IsNumber()
  @IsNotEmpty()
  @ApiProperty()
  readonly id: number;
}
