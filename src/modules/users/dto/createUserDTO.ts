import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsEmail, IsString, Length } from 'class-validator';
import { User } from '../entity/user.entity';

export class CreateUserDTO extends User {
  @IsDefined()
  @IsString()
  @IsEmail()
  @ApiProperty()
  readonly email: string;

  @IsDefined()
  @IsString()
  @Length(6, 255)
  @ApiProperty()
  readonly password: string;
}
