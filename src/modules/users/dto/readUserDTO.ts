import { ApiParam, ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { User } from '../entity/user.entity';

export class ReadUserDTO extends User {
  @IsNumber()
  @IsNotEmpty()
  @ApiProperty()
  readonly id: number;
}
