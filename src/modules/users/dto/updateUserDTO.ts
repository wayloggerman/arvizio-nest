import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsString, Length } from 'class-validator';
import { User } from '../entity/user.entity';

export class UpdateUserDTO extends User {
  @IsDefined()
  @IsString()
  @Length(8, 255)
  @ApiProperty()
  phone: string;

  @IsDefined()
  @IsString()
  @Length(3, 255)
  @ApiProperty()
  name: string;

  @IsDefined()
  @IsString()
  @Length(10, 255)
  @ApiProperty()
  password: string;
}
