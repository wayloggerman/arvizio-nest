import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MailerModule } from 'src/modules/mailer/mailer.module';
import { MailerService } from 'src/modules/mailer/mailer.service';
import { NanoidService } from 'src/services/nanoid.service';
import { UserController } from './controllers/user.controller';
import { User } from './entity/user.entity';
import { UserService } from './services/user.service';
import urlConfig from 'src/config/url.config';
import sessionConfig from 'src/config/session.config';
import { Session } from './entity/session.entity';
import { SessionService } from './services/session.service';
import { SessionController } from './controllers/session.controller';
import authConfig from 'src/config/auth.config';
import { VKStrategy } from './strategies/vk.strategy';
import { YAStrategy } from './strategies/ya.strategy';
import { GOStrategy } from './strategies/go.strategy';
@Module({
  imports: [
    TypeOrmModule.forFeature([User, Session]),
    MailerModule,
    ConfigModule.forFeature(urlConfig),
    ConfigModule.forFeature(sessionConfig),
    ConfigModule.forFeature(authConfig),
  ],
  controllers: [UserController, SessionController],
  providers: [
    UserService,
    NanoidService,
    MailerService,
    ConfigService,
    SessionService,
    VKStrategy,
    YAStrategy,
    GOStrategy,
  ],
  exports: [UserService, SessionService],
})
export class UsersModule {}
