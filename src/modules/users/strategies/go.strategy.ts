import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-google-oauth20';

import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { UserService } from '../services/user.service';
import { UserSource } from '../entity/user.enum';
import { passportValidate } from './auth.handlers';

@Injectable()
export class GOStrategy extends PassportStrategy(Strategy, 'go') {
  constructor(
    private readonly configService: ConfigService,
    private readonly userService: UserService,
  ) {
    super(
      {
        clientID: configService.get('auth.go.clientId'),
        clientSecret: configService.get('auth.go.clientSe'),
        callbackURL: 'https://360x-backend.arvizio.com/session/auth/go',
        scope: ['profile', 'email'],
      },
      (
        accessToken: any,
        refreshToken: any,
        params: any,
        profile: any,
        cb: any,
      ) => {
        this.validate(accessToken, refreshToken, profile, cb);
      },
    );
  }

  async validate(
    accessToken: string,
    refreshToken: string,
    profile: any,
    done: any,
  ): Promise<any> {
    return passportValidate(
      this.userService,
      accessToken,
      refreshToken,
      profile,
      done,
      UserSource.GOOGLE,
    );
  }
}
