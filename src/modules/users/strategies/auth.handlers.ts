import { UserSource } from '../entity/user.enum';

export async function passportValidate(
  userService: any,
  accessToken: string,
  refreshToken: string,
  profile: any,
  done: any,
  source: UserSource,
): Promise<any> {
  const name = profile.displayName;
  const email = profile.emails[0].value ?? profile.username;
  const photo = profile.photos[0].value;

  const user = await userService.read({ externalId: profile.id });
  if (user.length) {
    profile.id = user[0].id;
    return done(null, profile);
  }

  const newUser = await userService.create(
    {
      name,
      photo,
      email,
      source,
      externalId: profile.id,
      isConfirmed: true,
    },
    false,
  );
  profile.id = newUser?.id ?? profile.user.id;
  done(null, profile);
}
