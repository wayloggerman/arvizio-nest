import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-yandex';

import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { UserService } from '../services/user.service';
import { UserSource } from '../entity/user.enum';
import { passportValidate } from './auth.handlers';

@Injectable()
export class YAStrategy extends PassportStrategy(Strategy, 'ya') {
  constructor(
    private readonly configService: ConfigService,
    private readonly userService: UserService,
  ) {
    super(
      {
        clientID: configService.get('auth.ya.clientId'),
        clientSecret: configService.get('auth.ya.clientSe'),
        callbackURL: 'https://360x-backend.arvizio.com/session/auth/ya',
      },
      (
        accessToken: any,
        refreshToken: any,
        params: any,
        profile: any,
        cb: any,
      ) => {
        this.validate(accessToken, refreshToken, profile, cb);
      },
    );
  }

  async validate(
    accessToken: string,
    refreshToken: string,
    profile: any,
    done: any,
  ): Promise<any> {
    return passportValidate(
      this.userService,
      accessToken,
      refreshToken,
      profile,
      done,
      UserSource.YANDEX,
    );
  }
}
