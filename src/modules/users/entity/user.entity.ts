import { Injectable } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { DateFnsService } from 'src/services/date-fns.service';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { UserSource } from './user.enum';

@Entity('user')
@Injectable()
export class User {
  @PrimaryGeneratedColumn({ name: 'id' })
  id: number;

  @Column({
    name: 'email',
    nullable: true,
    unique: true,
  })
  email: string;
  @Column({
    name: 'password',
    nullable: false,
  })
  password: string;

  @Column({
    name: 'is_confirmed',
    nullable: false,
    type: 'boolean',
    default: false,
  })
  isConfirmed: boolean;

  @Column({
    name: 'banned',
    nullable: false,
    type: 'boolean',
    default: false,
  })
  banned: boolean;

  @Column({
    name: 'name',
    nullable: true,
    type: 'varchar',
    length: 255,
  })
  name: string;
  @Column({
    name: 'phone',
    nullable: true,
    type: 'varchar',
    length: 255,
  })
  phone: string;

  @Column({
    name: 'confirm_token',
    type: 'varchar',
    length: 255,
    default: '',
  })
  confirmToken: string;

  @Column({
    name: 'created_at',
    type: 'varchar',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;
  @Column({
    name: 'updated_at',
    type: 'varchar',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;
  @Column({
    name: 'deleted_at',
    type: 'varchar',
    nullable: true,
  })
  deletedAt: Date;

  @Column({
    name: 'source',
    type: 'varchar',
    default: UserSource.MAIL,
  })
  source: UserSource;

  @Column({
    name: 'external_id',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  externalId: string;

  @Column({
    type: 'varchar',
    nullable: true,
    length: 8096,
  })
  photo: string;
}
