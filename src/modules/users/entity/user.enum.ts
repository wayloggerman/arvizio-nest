export enum UserType {
  USER = 'user',
  ADMIN = 'admin',
}

export enum UserSource {
  MAIL = 'mail',
  VK = 'vk',
  GOOGLE = 'google',
  YANDEX = 'yandex',
}
