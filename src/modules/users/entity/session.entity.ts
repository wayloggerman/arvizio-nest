import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity('session')
export class Session {
  @PrimaryGeneratedColumn()
  id: number;
  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn({ name: 'user_id' })
  userId: number;

  @Column({
    name: 'token',
    type: 'varchar',
    length: 255,
  })
  token: string;

  @Column({
    name: 'created_at',
    type: 'varchar',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;
  @Column({
    name: 'updated_at',
    type: 'varchar',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;
  @Column({
    name: 'deleted_at',
    type: 'varchar',
    nullable: true,
  })
  deletedAt: Date;
}
