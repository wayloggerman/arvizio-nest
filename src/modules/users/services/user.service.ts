import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as nanoid from 'nanoid';
import { MailerService } from 'src/modules/mailer/mailer.service';
import { NanoidService } from 'src/services/nanoid.service';
import { Repository } from 'typeorm';
import { CheckUserDTO } from '../dto/checkUserDTO';
import { CreateUserDTO } from '../dto/createUserDTO';
import { UpdateUserDTO } from '../dto/updateUserDTO';
import { User } from '../entity/user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly mailerService: MailerService,
    private readonly nanoidService: NanoidService,
  ) {}

  async create(user: Partial<CreateUserDTO>, needConfirm = true) {
    const confirmToken = await this.nanoidService.generate();
    const newUser = await this.userRepository.insert({
      ...user,
      email: user.email,
      password: user.password ?? nanoid.nanoid(),
      confirmToken,
    });

    if (user.email && needConfirm)
      await this.mailerService.confirmMail(user.email, confirmToken);

    const res = newUser.identifiers[0].id;
    const newUserResponse: Partial<User | null> =
      await this.userRepository.findOne({
        where: {
          id: res,
        },
      });
    delete newUserResponse?.password;
    delete newUserResponse?.confirmToken;
    return newUserResponse;
  }

  async newPassword(email: string) {
    const findUser = await this.userRepository.findOne({
      select: {
        id: true,
      },
      where: {
        email,
      },
    });

    if (!findUser) return null;
    const newPassword = await this.nanoidService.generate();

    const res = await this.userRepository.update(
      {
        email,
      },
      {
        password: newPassword,
      },
    );

    await this.mailerService.newPassword(email, newPassword);
    return res;
  }

  async update(user: UpdateUserDTO) {
    const forUpdate: any = {
      password: user.password ? () => `${user.password}` : undefined,
      salt: user.password ? () => `gen_salt('md5')` : undefined,
      name: user.name,
      phone: user.phone,
    };

    Object.keys(forUpdate).forEach((key) => {
      if (forUpdate[key] === undefined) {
        delete forUpdate[key];
      }
    });

    const updatedUser = await this.userRepository.update(
      {
        id: user.id,
      },
      {
        ...forUpdate,
      },
    );

    return updatedUser;
  }

  async read(readUserDTO: Partial<User>) {
    return this.userRepository.find({
      where: {
        ...readUserDTO,
        isConfirmed: true,
      },
    });
  }

  async readById(id: number) {
    return this.userRepository.find({
      where: {
        id,
        isConfirmed: true,
      },
    });
  }

  async delete(deleteUserDTO: Partial<User>) {
    return this.userRepository.delete({
      id: deleteUserDTO.id,
    });
  }

  async confirm(token: string) {
    return this.userRepository.update(
      {
        confirmToken: token,
      },
      {
        isConfirmed: true,
      },
    );
  }

  async check(dto: CheckUserDTO) {
    const res = await this.userRepository.query(
      `SELECT * FROM "user" WHERE email = $1 AND password = $2`,
      [dto.email, dto.password],
    );

    return res;
  }
}
