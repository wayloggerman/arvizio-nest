import { Injectable } from '@nestjs/common';
import { Session } from '../entity/session.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { NanoidService } from 'src/services/nanoid.service';
import { Repository } from 'typeorm';

@Injectable()
export class SessionService {
  constructor(
    @InjectRepository(Session)
    private readonly sessionRepository: Repository<Session>,
    private readonly nanoidService: NanoidService,
  ) {}
  async create(userId: number) {
    const sessionId = await this.nanoidService.generate();

    this.sessionRepository.insert({
      token: sessionId,
      userId,
    });

    return sessionId;
  }
  async read(token: string) {
    const res = await this.sessionRepository.findOne({
      relations: ['userId'],
      select: {
        id: true,
        userId: true,
      },
      where: {
        token,
      },
    });

    return res;
  }
  delete(token: string) {
    return this.sessionRepository.delete({
      token,
    });
  }
}
