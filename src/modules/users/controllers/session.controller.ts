import {
  Body,
  Controller,
  Get,
  Post,
  Redirect,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { CheckUserDTO } from '../dto/checkUserDTO';
import { Response } from 'express';
import { UserService } from '../services/user.service';
import { SessionService } from '../services/session.service';
import { ConfigService } from '@nestjs/config';
import { AuthGuard } from 'src/guards/auth.guard';
import { User } from '../entity/user.entity';

import * as NestPassport from '@nestjs/passport';

@Controller('session')
export class SessionController {
  constructor(
    private readonly userService: UserService,
    private readonly sessionService: SessionService,
    private readonly configService: ConfigService,
  ) {}

  private setCookie(response: Response, session: string) {
    response.cookie(
      this.configService.get('session.name') ?? 'session_def',
      session,
      {
        httpOnly: true,
      },
    );
  }
  @UseGuards(AuthGuard)
  @Get()
  async token(@Res() response: Response, @Req() request: any) {
    delete request.user.password;
    delete request.user.confirmToken;
    return response.status(200).json(request.user);
  }

  @UseGuards(AuthGuard)
  @Post('logout')
  async logout(
    @Req() request: any,
    @Res()
    response: Response,
  ) {
    const token = request.token;
    response.clearCookie(
      this.configService.get('session.name') ?? 'session_def',
    );
    return this.sessionService.delete(token);
  }

  @Post('check')
  async check(
    @Res()
    response: Response,
    @Body() checkUserDTO: CheckUserDTO,
  ) {
    const check = await this.userService.check(checkUserDTO);

    if (!check.length) {
      return response.status(401).send('User not found');
    }

    const user: Partial<User> = await this.userService
      .readById(check[0].id)
      .then((users) => Object.assign({}, users[0]));

    const session = await this.sessionService.create(check[0].id);
    this.setCookie(response, session);
    delete user.password;
    delete user.confirmToken;

    return response.status(200).json(user);
  }

  @UseGuards(NestPassport.AuthGuard('go'))
  @Redirect()
  @Get('auth/go')
  async google(
    @Req() request: any,
    @Res({
      passthrough: true,
    })
    response: Response,
  ) {
    return this.authCallback(request, response);
  }

  @UseGuards(NestPassport.AuthGuard('vk'))
  @Get('auth/vk')
  @Redirect()
  async vk(
    @Req() request: any,
    @Res({
      passthrough: true,
    })
    response: Response,
  ) {
    return this.authCallback(request, response);
  }

  @UseGuards(NestPassport.AuthGuard('ya'))
  @Get('auth/ya')
  @Redirect()
  async yandex(
    @Req() request: any,
    @Res({
      passthrough: true,
    })
    response: Response,
  ) {
    return this.authCallback(request, response);
  }

  private async authCallback(request: any, response: any) {
    const user = request.user;

    if (!user) return { url: this.configService.get('url.frontend') };

    const session = await this.sessionService.create(user.id);
    this.setCookie(response, session);
    return { url: this.configService.get('url.frontend') };
  }
}
