import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Redirect,
  Res,
  UseGuards,
} from '@nestjs/common';
import { response, Response } from 'express';
import { ConfigService } from '@nestjs/config';
import { AuthGuard } from 'src/guards/auth.guard';
import { CheckUserDTO } from '../dto/checkUserDTO';
import { CreateUserDTO } from '../dto/createUserDTO';
import { UpdateUserDTO } from '../dto/updateUserDTO';
import { UserService } from '../services/user.service';
import { SessionService } from '../services/session.service';

@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly sessionService: SessionService,
    private readonly configService: ConfigService,
  ) {}

  @Post()
  async create(@Body() createUserDTO: CreateUserDTO) {
    return this.userService.create(createUserDTO);
  }

  @UseGuards(AuthGuard)
  @Get()
  read(@Query('id') readUserDTO: number) {
    return this.userService.read({
      id: readUserDTO,
    });
  }

  @UseGuards(AuthGuard)
  @Patch()
  update(@Body() updateUserDTO: UpdateUserDTO) {
    return this.userService.update(updateUserDTO);
  }

  @UseGuards(AuthGuard)
  @Delete()
  delete(@Query('id') deleteUserDTO: number) {
    return this.userService.delete({ id: deleteUserDTO });
  }

  @Get('newPassword/:mail')
  async newPassword(@Param('mail') mail: string, @Res() response: Response) {
    const res = await this.userService.newPassword(mail);
    if (!res) return response.status(404).send('User not found');
    return response.status(204).send('New password sent');
  }

  @Get('confirm')
  @Redirect()
  async confirm(@Query('token') token: string) {
    await this.userService.confirm(token);
    return {
      url: this.configService.get('url.frontend'),
    };
  }
}
