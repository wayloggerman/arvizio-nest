import { Media } from 'src/modules/media/entities/media.entity';
import { User } from 'src/modules/users/entity/user.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('spot')
export class Spot {
  @PrimaryGeneratedColumn()
  id: number;
  @ManyToOne(() => Media, (media) => media.id)
  @JoinColumn({ name: 'source' })
  source: number | Partial<Media>;

  @ManyToOne(() => Media, (media) => media.id)
  @JoinColumn({ name: 'target' })
  target: number | Partial<Media>;
  @Column({
    name: 'x',
    type: 'real',
    default: 0,
  })
  x: number;
  @Column({
    name: 'y',
    type: 'real',
    default: 0,
  })
  y: number;
  @Column({
    name: 'z',
    type: 'real',
    default: -800,
  })
  z: number;
  @Column({
    name: 'created_at',
    type: 'varchar',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;
  @Column({
    name: 'updated_at',
    type: 'varchar',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;
  @Column({
    name: 'deleted_at',
    type: 'varchar',
    nullable: true,
  })
  deletedAt: Date;

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn({ name: 'owner' })
  owner: number;
}
