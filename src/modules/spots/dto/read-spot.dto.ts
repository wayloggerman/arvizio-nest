import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDefined,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export default class ReadSpotDTO {
  @IsDefined()
  @IsOptional()
  @ApiPropertyOptional()
  projectId: string;
}
