import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';
import { Spot } from '../entiy/spot.entity';

export class CreateSpotDTO extends Spot {
  @ApiProperty()
  @IsNumber()
  target: number;

  @ApiProperty()
  @IsNumber()
  source: number;

  @ApiProperty()
  @IsNumber()
  x: number;

  @ApiProperty()
  @IsNumber()
  y: number;

  @ApiProperty()
  @IsNumber()
  z: number;
}
