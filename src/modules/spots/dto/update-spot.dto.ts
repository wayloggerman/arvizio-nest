import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsDefined, IsNumber } from 'class-validator';
import { Spot } from '../entiy/spot.entity';

export class UpdateSpotDTO extends Spot {
  @ApiProperty()
  @IsNumber()
  @IsDefined()
  id: number;

  @ApiPropertyOptional()
  @IsNumber()
  @IsDefined()
  x: number;

  @ApiPropertyOptional()
  @IsNumber()
  @IsDefined()
  y: number;

  @ApiPropertyOptional()
  @IsNumber()
  @IsDefined()
  z: number;
}
