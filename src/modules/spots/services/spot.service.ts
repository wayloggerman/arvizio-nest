import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { tr } from 'date-fns/locale';
import { IsNull, Repository } from 'typeorm';
import { CreateSpotDTO } from '../dto/create-spot.dto';
import ReadSpotDTO from '../dto/read-spot.dto';
import { UpdateSpotDTO } from '../dto/update-spot.dto';
import { Spot } from '../entiy/spot.entity';

@Injectable()
export class SpotService {
  constructor(
    @InjectRepository(Spot)
    private readonly spotRepository: Repository<Spot>,
  ) {}

  read(id: number, userId: number) {
    return this.spotRepository.findOne({
      where: {
        id,
        owner: userId,
      },
    });
  }

  readAll(userId: number, query: ReadSpotDTO) {
    const where = [];
    // if (!query.projectId) {
    //   where.push({
    //     owner: userId,
    //     deletedAt: IsNull(),
    //   });
    // }
    // if (query.projectId) {
    //   where.push({
    //     deletedAt: IsNull(),
    //     source: {
    //       project: {
    //         id: parseInt(query.projectId),
    //         public: true,
    //       },
    //     },
    //   });
    //   where.push({
    //     deletedAt: IsNull(),
    //     target: {
    //       project: {
    //         id: parseInt(query.projectId),
    //         public: true,
    //       },
    //     },
    //   });
    // }
    return this.spotRepository.find({
      relations: {
        source: true,
        target: true,
      },
      where: {
        deletedAt: IsNull(),
        owner: userId,
        source: {
          project: {
            id: parseInt(query.projectId),
          },
        },
      },
    });
  }

  create(dto: CreateSpotDTO, userId: number) {
    return this.spotRepository.insert({
      ...dto,
      owner: userId,
    });
  }

  update(id: number, dto: UpdateSpotDTO, userId: number) {
    return this.spotRepository.update(id, {
      ...dto,
      owner: userId,
    });
  }
  delete(id: number, userId: number) {
    return this.spotRepository.update(
      { id, owner: userId },
      { deletedAt: () => 'CURRENT_TIMESTAMP' },
    );
  }
  deleteBySourceAndTarget(sourceId: number, targetId: number, userId: number) {
    return this.spotRepository.update(
      { source: sourceId, target: targetId, owner: userId },
      { deletedAt: () => 'CURRENT_TIMESTAMP' },
    );
  }
}
