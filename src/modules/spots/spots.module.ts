import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MediaModule } from '../media/media.module';
import { UsersModule } from '../users/users.module';
import { SpotController } from './controllers/spot.controller';
import { Spot } from './entiy/spot.entity';
import { SpotService } from './services/spot.service';

@Module({
  imports: [UsersModule, TypeOrmModule.forFeature([Spot]), MediaModule],
  controllers: [SpotController],
  providers: [SpotService],
})
export class SpotsModule {}
