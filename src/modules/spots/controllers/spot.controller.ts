import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AnyMxRecord } from 'dns';
import { AuthGuard } from 'src/guards/auth.guard';
import { CreateSpotDTO } from '../dto/create-spot.dto';
import ReadSpotDTO from '../dto/read-spot.dto';
import { UpdateSpotDTO } from '../dto/update-spot.dto';
import { SpotService } from '../services/spot.service';

@UseGuards(AuthGuard)
@Controller('spot')
export class SpotController {
  constructor(private readonly spotService: SpotService) {}

  @Post()
  create(@Req() request: any, @Body() dto: CreateSpotDTO) {
    const userId = request.user.id;
    return this.spotService.create(dto, userId);
  }

  @Get()
  read(@Req() request: any, @Query('id') id: number) {
    const userId = request.user.id;
    return this.spotService.read(id, userId);
  }

  @Get('all')
  readAll(@Req() request: any, @Query() query: ReadSpotDTO) {
    const userId = request.user.id;
    return this.spotService.readAll(userId, query);
  }

  @Patch()
  update(@Req() request: any, @Body() dto: UpdateSpotDTO) {
    const userId = request.user.id;
    return this.spotService.update(dto.id, dto, userId);
  }

  @Delete()
  delete(@Req() request: any, @Query('id') id: number) {
    const userId = request.user.id;
    return this.spotService.delete(id, userId);
  }
  @Delete(':sourceId/:targetId')
  deleteBySourceAndTarget(
    @Req() request: any,
    @Param('sourceId') sourceId: number,
    @Param('targetId') targetId: number,
  ) {
    const userId = request.user.id;
    return this.spotService.deleteBySourceAndTarget(sourceId, targetId, userId);
  }
}
