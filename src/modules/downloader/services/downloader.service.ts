import { Injectable } from '@nestjs/common';
import { createReadStream } from 'fs';
import { readFile } from 'fs/promises';
import { FileService } from 'src/modules/files/services/file.services';
import * as _ from 'lodash';

@Injectable()
export class DownloaderService {
  constructor(private readonly fileService: FileService) {}

  async download(
    mediaId: number,
    fileId: number,
    userId: number,
    range?: {
      start: string;
      end: string;
    },
  ) {
    const file = parseInt(fileId as unknown as string);

    const fileEntity = _.isNaN(file)
      ? await this.fileService.readByName(
          mediaId,
          fileId as unknown as string,
          userId,
        )
      : await this.fileService.read(mediaId, fileId, userId);

    if (!fileEntity) return null;
    return {
      buffer: range
        ? createReadStream(fileEntity.path, {
            start: parseInt(range.start),
            end: fileEntity.size,
          })
        : createReadStream(fileEntity.path),
      name: fileEntity.name,
      type: fileEntity.type,
      size: fileEntity.size,
    };
  }
}
