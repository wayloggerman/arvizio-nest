import {
  Controller,
  Get,
  Param,
  Req,
  Res,
  StreamableFile,
  UseGuards,
} from '@nestjs/common';
import { DownloadGuard } from 'src/guards/download.guard';
// import { DownloadGuard } from 'src/guards/download.guard';
import { DownloaderService } from '../services/downloader.service';

@Controller('download')
// @UseGuards(DownloadGuard)
export class DownloaderController {
  constructor(private readonly downloaderService: DownloaderService) {}
  @Get('/:mediaId/:fileId')
  async getFile(
    @Req() request: any,
    @Res({ passthrough: true }) res: any,
    @Param('fileId') fileId: number,
    @Param('mediaId') mediaId: number,
  ): Promise<StreamableFile | null> {
    // ) {

    const range = request.headers.range ?? 'bytes=0-';

    const rangeParts = range.split('=')[1].split('-');

    const file = await this.downloaderService.download(
      mediaId,
      fileId,
      request.user?.id,
      rangeParts[1] ? { start: rangeParts[0], end: rangeParts[1] } : undefined,
    );

    if (!file) return null;
    let contentType = 'application/octet-stream';
    switch (file?.type) {
      case 'audio':
        contentType = 'audio/mpeg';
        break;
      case 'video':
        contentType = 'video/mp4';
        break;
    }

    res.set({
      'Content-Length': file.size,
      'Accept-Ranges': 'bytes',
      'Content-Ranges': `bytes ${rangeParts[0]}-${
        rangeParts[1] ? rangeParts[1] : file.size - 1
      }/${file.size}`,
      'Content-Type': contentType,
    });

    return new StreamableFile(file.buffer);
  }
}
