import { Module } from '@nestjs/common';
import { FilesModule } from '../files/files.module';
import { MediaModule } from '../media/media.module';
import { UsersModule } from '../users/users.module';
import { DownloaderController } from './controllers/downloader.controller';
import { DownloaderService } from './services/downloader.service';

@Module({
  imports: [UsersModule, FilesModule, MediaModule],
  providers: [DownloaderService],
  controllers: [DownloaderController],
})
export class DownloaderModule {}
