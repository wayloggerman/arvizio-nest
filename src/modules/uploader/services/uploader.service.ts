import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as fs from 'fs/promises';
import { ImageTransformerService } from 'src/media-transformer/services/image-transformer.service';
import { VideoTransformerService } from 'src/media-transformer/services/video-transformer.service';
import { FileTypeEnum } from 'src/modules/files/entity/file.enum';
import { FileService } from 'src/modules/files/services/file.services';
import { Media } from 'src/modules/media/entities/media.entity';
import { MediaType } from 'src/modules/media/entities/media.enum';
import { MediaService } from 'src/modules/media/media.service';

@Injectable()
export class UploaderService {
  constructor(
    private readonly fileService: FileService,
    private readonly mediaService: MediaService,
    private readonly LocalStorageConfig: ConfigService,
    private readonly videoTransformerService: VideoTransformerService,
    private readonly imageTransformerService: ImageTransformerService,
  ) {}

  async uploadFile(
    file: Express.Multer.File,
    userId: number,
    mediaId: number,
    fileId: number,
    name: string,
  ) {
    // находим файл заносим размер
    const fileEntity = await this.fileService.read(mediaId, fileId, userId);
    if (!fileEntity) return null;

    const mediaEntity = fileEntity.media as unknown as Media;
    const path = this.createPath(mediaId, userId);
    await this.preparePath(path);

    const extension = file.originalname.split('.').pop() ?? '.unknown';
    const pathWithFileName = `${path}/${fileEntity.id}`;

    await fs.writeFile(pathWithFileName, file.buffer);

    mediaEntity.size -= fileEntity.size; // обновляем размер медиа убирая размер прежнего файла под этим ид
    mediaEntity.size += file.size; // обновляем размер медиа добавляя размер нового файла
    await this.mediaService.update(mediaEntity, userId);
    await this.fileService.update(
      fileEntity.id,
      {
        ...fileEntity,
        id: fileEntity.id,
        size: file.size,
        name,
        path: pathWithFileName,
        extension: `.${extension}`,
      },
      userId,
    );
    const trumbs = [];
    if (mediaEntity.type === MediaType.video) {
      const videoTrumbs = await Promise.all([
        this.videoTransformerService.createScreenshot(
          pathWithFileName,
          'trumbnail_small',
        ),
        this.videoTransformerService.createScreenshot(
          pathWithFileName,
          'trumbnail_medium',
          '800x600',
        ),
      ]);

      trumbs.push(...videoTrumbs);
    }

    if (
      (mediaEntity.type === MediaType.image ||
        mediaEntity.type === MediaType.panorama ||
        mediaEntity.type === MediaType.pseudo3d) &&
      fileEntity.type !== FileTypeEnum.audio
    ) {
      const trumb = await this.imageTransformerService.createThumbnail(
        pathWithFileName,
        'trumbnail_small',
        360,
      );
      trumbs.push(trumb);
    }
    for (const trumb of trumbs) {
      const extension = '.' + trumb.split('.').pop() ?? '.unknown';
      const name = trumb.split('/').pop() ?? 'unknown';

      const file = await fs.readFile(trumb);
      await this.fileService.create(
        {
          media: mediaEntity.id,
          name,
          type: FileTypeEnum.trumb,
          extension,
          storage: 'local',
          size: file.length,
          path: trumb,
        },
        userId,
      );
    }
    return true;
  }

  async preparePath(path: string) {
    return fs.mkdir(path, { recursive: true });
  }

  createPath(mediaId: number, userId: number) {
    const uploadDir = this.LocalStorageConfig.get('local-storage.path');
    const path = `${uploadDir}/${userId}/media-${mediaId}`;
    return path;
  }
}
