import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { FilesModule } from '../files/files.module';
import { MediaModule } from '../media/media.module';
import { UsersModule } from '../users/users.module';
import { UploaderController } from './contollers/uploader.controller';
import { UploaderService } from './services/uploader.service';
import LocalStorageConfig from 'src/config/local-storage.config';
import { MediaTransformerModule } from 'src/media-transformer/media-transformer.module';

@Module({
  imports: [
    UsersModule,
    FilesModule,
    MediaModule,
    ConfigModule.forFeature(LocalStorageConfig),
    MediaTransformerModule,
  ],
  providers: [UploaderService],
  controllers: [UploaderController],
})
export class UploaderModule {}
