import {
  Body,
  Controller,
  Param,
  ParseFloatPipe,
  Post,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBody, ApiConsumes } from '@nestjs/swagger';
import { AuthGuard } from 'src/guards/auth.guard';
import { UploaderService } from '../services/uploader.service';
import { FileSizeValidationPipe } from '../validators/size.validator';

@UseGuards(AuthGuard)
@Controller('upload')
export class UploaderController {
  constructor(private readonly uploaderService: UploaderService) {}
  @Post('/:mediaId/:fileId')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseInterceptors(
    FileInterceptor('file', {
      limits: {
        fileSize: 150 * 1024 * 1024,
      },
    }),
  )
  uploadFile(
    @Req() request: any,
    @Param('mediaId') mediaId: number,
    @Param('fileId') fileId: number,
    @Body() uploadBody: any,
    @UploadedFile() file: any,
  ) {
    const userId = request.user.id;

    return this.uploaderService.uploadFile(
      file,
      userId,
      mediaId,
      fileId,
      uploadBody.fileName,
    );
  }
}
