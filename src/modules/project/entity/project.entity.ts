import { User } from 'src/modules/users/entity/user.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ProjectType } from './project.enum';

@Entity('project')
export class Project {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column({
    type: 'varchar',
    length: 8096,
  })
  description: string;
  @Column({
    type: 'varchar',
    length: 255,
  })
  type: ProjectType;
  @Column({
    name: 'created_at',
    type: 'varchar',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;
  @Column({
    name: 'updated_at',
    type: 'varchar',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedAt: Date;
  @Column({
    name: 'deleted_at',
    type: 'varchar',
    nullable: true,
  })
  deletedAt: Date;

  @Column({
    name: 'public',
    default: false,
    type: 'boolean',
  })
  public: boolean;

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn({ name: 'owner' })
  owner: number | { id: number };
}
