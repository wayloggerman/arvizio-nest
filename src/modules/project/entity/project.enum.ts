export enum ProjectType {
  gallery = 'gallery',
  video = 'video',
  audio = 'audio',
  pano = 'pano',
  dem3 = '3d',
  pseudo3d = 'pseudo3d',
}
