import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SessionService } from 'src/modules/users/services/session.service';
import { UsersModule } from 'src/modules/users/users.module';
import { MediaModule } from '../media/media.module';
import { ProjectController } from './controllers/project.controller';
import { Project } from './entity/project.entity';
import { ProjectService } from './services/project.service';

@Module({
  imports: [TypeOrmModule.forFeature([Project]), UsersModule, MediaModule],
  providers: [ProjectService],
  controllers: [ProjectController],
})
export class ProjectModule {}
