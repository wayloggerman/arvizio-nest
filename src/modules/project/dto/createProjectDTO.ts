import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsEnum, IsOptional, IsString } from 'class-validator';
import { Project } from '../entity/project.entity';
import { ProjectType } from '../entity/project.enum';

export class CreateProjectDTO extends Project {
  @IsDefined()
  @IsString()
  @ApiProperty()
  name: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  description: string;

  @IsDefined()
  @IsEnum(ProjectType)
  @ApiProperty()
  type: ProjectType;
}
