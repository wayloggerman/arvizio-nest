import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDefined,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class UpdateProjectDTO {
  @IsDefined()
  @IsNumber()
  @ApiProperty()
  id: number;

  @IsString()
  @IsOptional()
  @ApiProperty()
  name?: string;

  @IsString()
  @ApiProperty()
  @IsOptional()
  description?: string;

  @IsBoolean()
  @ApiProperty()
  @IsOptional()
  public?: boolean;

  @IsNumber()
  @ApiProperty()
  @IsOptional()
  owner?: number;
}
