import {
  Body,
  Controller,
  Delete,
  Get,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { AuthGuard } from 'src/guards/auth.guard';
import { CreateProjectDTO } from '../dto/createProjectDTO';
import ReadProjectDTO from '../dto/readProjectDTO';
import { UpdateProjectDTO } from '../dto/updateProjectDTO';
import { ProjectService } from '../services/project.service';

@UseGuards(AuthGuard)
@Controller('project')
export class ProjectController {
  constructor(private readonly projectService: ProjectService) {}

  @Get()
  async getProjects(
    @Req() request: Request | any,
    @Query() dto: ReadProjectDTO,
  ) {
    const projects = await this.projectService.read(dto, request.user.id);
    return projects;
  }

  @Post()
  createProject(@Req() request: Request | any, @Body() dtp: CreateProjectDTO) {
    return this.projectService.create(dtp, request.user.id);
  }

  @Patch()
  updateProject(@Req() request: Request | any, @Body() dto: UpdateProjectDTO) {
    return this.projectService.update(dto, request.user.id);
  }

  @Delete()
  deleteProject(@Req() request: Request | any, @Query('id') id: number) {
    return this.projectService.delete(id, request.user.id);
  }
}
