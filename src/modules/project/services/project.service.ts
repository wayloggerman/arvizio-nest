import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { create } from 'domain';
import { read } from 'fs';
import { FindOptionsWhere, IsNull, Repository } from 'typeorm';
import { CreateProjectDTO } from '../dto/createProjectDTO';
import ReadProjectDTO from '../dto/readProjectDTO';
import { UpdateProjectDTO } from '../dto/updateProjectDTO';
import { Project } from '../entity/project.entity';

import * as _ from 'lodash';
import { User } from 'src/modules/users/entity/user.entity';

@Injectable()
export class ProjectService {
  constructor(
    @InjectRepository(Project)
    private readonly projectRepository: Repository<Project>,
  ) {
    //crud
  }
  async create(dto: CreateProjectDTO, userId: number) {
    const res = await this.projectRepository.insert({
      name: dto.name,
      description: dto.description,
      type: dto.type,
      owner: userId,
    });

    return res.identifiers[0].id;
  }
  async read(dto: ReadProjectDTO, userId: number) {
    const where: FindOptionsWhere<Partial<Project>>[] = [];

    console.log({ dto });

    where.push({
      owner: {
        id: userId,
      },
      id: dto.id ? dto.id : undefined,
      deletedAt: IsNull(),
    });

    console.log({
      where,
    });

    const projects = await this.projectRepository.find({
      where,
      relations: {
        owner: true,
      },
    });

    projects.forEach((project) => {
      project.owner = (project.owner as User).id;
    });
    return projects;
  }
  update(dto: UpdateProjectDTO, userId: number) {
    return this.projectRepository.update(
      {
        id: dto.id,
        owner: userId,
      },
      {
        ...dto,
      },
    );
  }
  delete(id: number, userId: number) {
    return this.projectRepository.update(
      {
        id,
        owner: userId,
      },
      {
        deletedAt: () => `CURRENT_TIMESTAMP`,
      },
    );
  }
}
