import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { Observable } from 'rxjs';
import { MediaService } from 'src/modules/media/media.service';
import { SessionService } from 'src/modules/users/services/session.service';

@Injectable()
export class DownloadGuard implements CanActivate {
  constructor(
    private readonly configService: ConfigService,
    private readonly sessionService: SessionService,
    private readonly mediaService: MediaService,
  ) {}
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();

    return validateRequest(
      request,
      this.configService,
      this.sessionService,
      this.mediaService,
    );
  }
}
async function validateRequest(
  request: any,
  config: any,
  sessionService: SessionService,
  mediaService: MediaService,
): Promise<boolean> {
  const cookieName = config.get('session.name');

  const mediaId = parseInt(request.params.mediaId);

  if (!mediaId) return false;

  // check if media is public
  const media = await mediaService.findOne(mediaId);

  if (media) {
    request.user = media.owner.id;

    return true;
  }

  const cookie = request.cookies[cookieName];

  if (!cookie) return false;

  const session = await sessionService.read(cookie);

  if (!session) return false;
  request.user = session.userId;
  request.token = cookie;

  return true;
}
