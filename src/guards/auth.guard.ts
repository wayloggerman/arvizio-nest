import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { log } from 'console';
import { Observable } from 'rxjs';
import { SessionService } from 'src/modules/users/services/session.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly configService: ConfigService,
    private readonly sessionService: SessionService,
  ) {}
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();

    return validateRequest(request, this.configService, this.sessionService);
  }
}
async function validateRequest(
  request: any,
  config: any,
  sessionService: SessionService,
): Promise<boolean> {
  const cookieName = config.get('session.name');
  const cookie = request.cookies[cookieName];
  console.log({
    cookie,
    cookieName,
    req: request.cookies,
  });

  if (!cookie) {
    return false;
  }

  const session = await sessionService.read(cookie);
  if (!session) {
    return false;
  }

  request.user = session.userId;
  request.token = cookie;

  console.log({
    session,
    user: request.user,
  });

  return true;
}
