import { registerAs } from '@nestjs/config';

export default registerAs('app', () => ({
  dateFormat: process.env.DATE_FORMAT,
}));
