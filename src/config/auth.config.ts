import { registerAs } from '@nestjs/config';

export default registerAs('auth', () => {
  return {
    vk: {
      clientId: process.env.VK_CLIENT_ID,
      clientSe: process.env.VK_CLIENT_SE,
      Url: process.env.VK_URL,
    },
    ya: {
      clientId: process.env.YA_CLIENT_ID,
      clientSe: process.env.YA_CLIENT_SE,
      Url: process.env.YA_URL,
    },
    go: {
      clientId: process.env.GO_CLIENT_ID,
      clientSe: process.env.GO_CLIENT_SE,
      Url: process.env.GO_URL,
    },
  };
});
