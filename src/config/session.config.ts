import { registerAs } from '@nestjs/config';

export default registerAs('session', () => {
  return {
    name: process.env.SESSION_NAME,
  };
});
