import { registerAs } from '@nestjs/config';

export default registerAs('local-storage', () => ({
  path: process.env.LOCAL_STORAGE_PATH,
}));
