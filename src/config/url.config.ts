import { registerAs } from '@nestjs/config';

export default registerAs('url', () => ({
  userConfirm: process.env.USER_CONFIRM_URL,
  newPassword: process.env.NEW_PASSWORD_URL,
  frontend: process.env.FRONTEND_URL,
}));
