import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import dateFns from 'date-fns';

@Injectable()
export class DateFnsService {
  constructor(private readonly config: ConfigService) {}
  now() {
    const format = this.config.get('app.dateFormat');
    return dateFns.format(new Date(), format);
  }
}
