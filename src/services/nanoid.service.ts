import { Injectable } from '@nestjs/common';
import { nanoid } from 'nanoid';

@Injectable()
export class NanoidService {
  async generate() {
    const id = nanoid();
    return id;
  }
}
